# C-Dogs SDL:

This is a flatpak (WIP) for [C-DOGS SDL](https://github.com/cxong/cdogs-sdl)

## Current issues:

* The runtime used has to be 1.6 because the application fails to compile with
  a few compiler errors (because of -Werror) on later compilers

* The appstream included in the game doesn't contain "\<oars\>" and "\<releases\>"

## How to test:

### Clone the repository:
    git clone https://gitlab.com/Rampoina/com.cxong.cdogs-sdl.git

### Build the flatpak:
    flatpak-builder --force-clean build-dir com.cxong.cdogs-sdl.json

### Run the flatpak:
    flatpak-builder --run build-dir com.cxong.cdogs-sdl.json cdogs-sdl
